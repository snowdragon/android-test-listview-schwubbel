package com.example.twiter;

import java.util.ArrayList;

import net.xerael.test.listview.R;
import android.os.Bundle;
import android.app.ListActivity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends ListActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		final Button b = (Button) findViewById(R.id.addBtn);
		
		final ArrayList<String> aList = new ArrayList<String>();
		final ArrayAdapter<String> adapter =
				new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, aList);
		setListAdapter(adapter);
		
		b.setOnClickListener(new OnClickListener() {
			Toast t = Toast.makeText(getApplicationContext(),
					R.string.toast, Toast.LENGTH_SHORT);
			int i = 0;
			@Override
			public void onClick(View v) {
				aList.add("Schwubbeldi " + i++);
				t.show();
				adapter.notifyDataSetChanged();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
